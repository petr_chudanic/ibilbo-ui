import React from 'react';
import {render, unmountComponentAtNode} from "react-dom";
import {act} from "react-dom/test-utils";
import MainPage from '../components/main-page';
import Employee from "../model/employee";

let container = null;
beforeEach(() => {
    // setup a DOM element as a render target
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    // cleanup on exiting
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});

it('test main page initial state', async () => {
    const positions = [];
    const selectedEmployee = undefined;
    const employeeList = [
        new Employee("Adam", "Novak", "full-stack developer", new Date()),
        new Employee("Bivoj", "Svoboda", "front-end developer", new Date()),
        new Employee("Cecil", "Kovar", "sw admin", new Date())
    ];
    const employeeSelected = jest.fn((e) => e);
    const deleteEmployee = jest.fn((e) => e);
    const updateEmployee = jest.fn((e) => e);

    await act(async () => {
        render(<MainPage employeeList={employeeList}
                         employeeSelected={employeeSelected}
                         selectedEmployee={selectedEmployee}
                         deleteEmployee={deleteEmployee}
                         positions={positions}
                         updateEmployee={updateEmployee}/>, container);
    });

    const employeeEntries = container.querySelectorAll(".employeeEntry");
    expect(employeeEntries[0].querySelector("button").textContent).toBe("Delete");
    expect(employeeEntries[1].querySelector("button").textContent).toBe("Delete");
    expect(employeeEntries[2].querySelector("button").textContent).toBe("Delete");

    expect(employeeEntries[0].querySelectorAll(".content")[1].textContent).toBe("Adam Novak");
    expect(employeeEntries[1].querySelectorAll(".content")[1].textContent).toBe("Bivoj Svoboda");
    expect(employeeEntries[2].querySelectorAll(".content")[1].textContent).toBe("Cecil Kovar");

    expect(container.querySelector(".addNew").textContent).toBe("Add new");
    expect(container.querySelector(".employeeDetail").textContent).toBe("Select employee...");
});


it('test main page selected second employee', async () => {
    const positions = ["full-stack developer","front-end developer","sw admin","help desk","scrum master","product manager"];
    const employeeList = [
        new Employee("Adam", "Novak", "full-stack developer", new Date()),
        new Employee("Bivoj", "Svoboda", "front-end developer", new Date()),
        new Employee("Cecil", "Kovar", "sw admin", new Date())
    ];
    const selectedEmployee = employeeList[1];
    const employeeSelected = jest.fn((e) => e);
    const deleteEmployee = jest.fn((e) => e);
    const updateEmployee = jest.fn((e) => e);

    await act(async () => {
        render(<MainPage employeeList={employeeList}
                         employeeSelected={employeeSelected}
                         selectedEmployee={selectedEmployee}
                         deleteEmployee={deleteEmployee}
                         positions={positions}
                         updateEmployee={updateEmployee}/>, container);
    });

    expect(container.querySelector(".employeeDetail input[name='firstName']").value).toBe("Bivoj");
    expect(container.querySelector(".employeeDetail input[name='lastName']").value).toBe("Svoboda");
    expect(container.querySelector(".employeeDetail select[name='workPosition']").value).toBe("front-end developer");
    expect(container.querySelector(".employeeDetail input[name='dateOfBirth']").value).toBe(employeeList[1].getDateOfBirth());

    expect(container.querySelector(".employeeDetail button").textContent).toBe("Save");
});
