import React from 'react';
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import App from '../components/App';
import Employee from "../model/employee";

let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it('test main application window', async () => {

  await act(async () => {
    render(<App/>, container);
  });

  const employeeEntries = container.querySelectorAll(".employeeEntry");
  expect(employeeEntries[0].querySelector("button").textContent).toBe("Delete");
  expect(employeeEntries[1].querySelector("button").textContent).toBe("Delete");
  expect(employeeEntries[2].querySelector("button").textContent).toBe("Delete");

  expect(employeeEntries[0].querySelectorAll(".content")[1].textContent).toBe("Adam Novak");
  expect(employeeEntries[1].querySelectorAll(".content")[1].textContent).toBe("Bivoj Svoboda");
  expect(employeeEntries[2].querySelectorAll(".content")[1].textContent).toBe("Cecil Kovar");

  expect(container.querySelector(".addNew").textContent).toBe("Add new");
  expect(container.querySelector(".employeeDetail").textContent).toBe("Select employee...");

});

it('test navigation to new employee form', async () => {
  await act(async () => {
    render(<App/>, container);
  });

  act(() => {
    container.querySelector(".addNew").dispatchEvent(new MouseEvent("click", {bubbles: true}));
  });
  expect(container.querySelector("h1").innerHTML).toBe("Add new employee");
});

it('test selectedEmployee method', async () => {
  const appRef = React.createRef();

  await act(async () => {
    render(<App ref={appRef}/>, container);
  });

  expect(appRef.current.state.selectedEmployee).toBe(undefined);

  act(() => {
    appRef.current.employeeSelected(appRef.current.state.employeeList[0]);
  });

  expect(appRef.current.state.selectedEmployee).toBe(appRef.current.state.employeeList[0]);
});

it('test that positions were loaded in componentDidMount', async () => {
  const appRef = React.createRef();

  await act(async () => {
    render(<App ref={appRef}/>, container);
  });

  expect(appRef.current.state.positions).toStrictEqual(["full-stack developer", "front-end developer", "sw admin", "help desk", "scrum master", "product manager"]);
});

it('test deleteEmployee method', async () => {
  const appRef = React.createRef();

  await act(async () => {
    render(<App ref={appRef}/>, container);
  });

  const originalEmployeeList = appRef.current.state.employeeList;

  act(() => {
    appRef.current.employeeSelected(originalEmployeeList[0]);
  });

  expect(appRef.current.state.selectedEmployee).toBe(originalEmployeeList[0]);

  act(() => {
    appRef.current.deleteEmployee(originalEmployeeList[0]);
  });

  expect(appRef.current.state.selectedEmployee).toBe(undefined);
  expect(appRef.current.state.employeeList).toStrictEqual(originalEmployeeList.slice(1));
});

it('test addEmployee method', async () => {
  const appRef = React.createRef();

  await act(async () => {
    render(<App ref={appRef}/>, container);
  });

  const originalEmployeeList = appRef.current.state.employeeList;

  const newEmployee = new Employee("Dalibor", "Jandak", "sw admin", new Date());
  act(() => {
    appRef.current.addEmployee(newEmployee);
  });

  expect(appRef.current.state.selectedEmployee).toBe(undefined);
  expect(appRef.current.state.employeeList).toStrictEqual([...originalEmployeeList, newEmployee]);
});

it('test updateEmployee method', async () => {
  const appRef = React.createRef();

  await act(async () => {
    render(<App ref={appRef}/>, container);
  });

  const originalEmployeeList = appRef.current.state.employeeList;

  const updatedEmployee = new Employee("Dalibor", "Jandak", "sw admin", new Date());
  act(() => {
    appRef.current.updateEmployee(originalEmployeeList[0], updatedEmployee);
  });

  expect(appRef.current.state.selectedEmployee).toBe(updatedEmployee);
  expect(appRef.current.state.employeeList).toStrictEqual([updatedEmployee, ...originalEmployeeList.slice(1)]);
});