import * as React from "react";
import EmployeeForm from "./employee-form";

export default class EmployeeDetail extends React.Component {

    saveChanges = (employee) => {
        this.props.updateEmployee(this.props.selectedEmployee, employee);
    };

    render() {
        return <div className="employeeDetail">
            {this.props.selectedEmployee ? (
                    <div>
                        <EmployeeForm submitAction={this.saveChanges} positions={this.props.positions} employee={this.props.selectedEmployee}/>
                    </div>)
                :
                (<span>Select employee...</span>)}

        </div>
    }
}