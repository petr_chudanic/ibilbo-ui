import React from 'react';
import axios from 'axios';
import {BrowserRouter, Route} from "react-router-dom";
import Employee from '../model/employee';
import NewEmployee from "./new-employee";
import MainPage from "./main-page";
import {Container} from "semantic-ui-react";


class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            positions: [],
            selectedEmployee: undefined,
            employeeList: [
                new Employee("Adam", "Novak", "full-stack developer", new Date()),
                new Employee("Bivoj", "Svoboda", "front-end developer", new Date()),
                new Employee("Cecil", "Kovar", "sw admin", new Date())
            ]
        }
    }

    async componentDidMount() {
        const response = await axios.get("http://ibillboard.com/api/positions").catch((error) => {
            console.error(error);
            return {
                data: {
                    positions: []
                }
            }
        });
        this.setState({positions: response.data.positions});
    }

    employeeSelected = (employee) => {
        this.setState({
            selectedEmployee: employee
        })
    };

    deleteEmployee = (deletedEmployee) => {
        const employeeToDeleteIndex = this.state.employeeList.findIndex(employee =>
            employee.getId() === deletedEmployee.getId()
        );
        const newEmployeeList = [...this.state.employeeList];
        newEmployeeList.splice(employeeToDeleteIndex, 1);

        this.setState({
            employeeList: newEmployeeList,
            selectedEmployee: undefined
        });
    };

    addEmployee = (newEmployee) => {
        this.setState({
            employeeList: [...this.state.employeeList, newEmployee]
        });

    };

    updateEmployee = (originalEmployee, updatedEmployee) => {
        const employeeToUpdateIndex = this.state.employeeList.findIndex(employee =>
            employee.getId() === originalEmployee.getId()
        );
        const newEmployeeList = [...this.state.employeeList];
        newEmployeeList.splice(employeeToUpdateIndex, 1, updatedEmployee);

        this.setState({
            employeeList: newEmployeeList,
            selectedEmployee: updatedEmployee
        });
    };

    render() {
        return <Container>
            <BrowserRouter>
                <Route path="/" exact render={() =>
                    <MainPage employeeList={this.state.employeeList}
                              employeeSelected={this.employeeSelected}
                              selectedEmployee={this.state.selectedEmployee}
                              deleteEmployee={this.deleteEmployee}
                              positions={this.state.positions}
                              updateEmployee={this.updateEmployee}/>}/>
                <Route path="/new" exact render={() =>
                    <NewEmployee positions={this.state.positions} addEmployee={this.addEmployee}/>
                }/>
            </BrowserRouter>

        </Container>
    }
}

export default App;
