import * as React from "react";
import {Button, Grid, Header} from "semantic-ui-react";
import EmployeeList from "./employee-list";
import EmployeeDetail from "./employee-detail";
import {Redirect} from "react-router-dom";

export default class MainPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            redirectToNew: false
        }
    }

    redirectToNew = () => {
        this.setState({
            redirectToNew: true
        })
    };

    render() {
        if (this.state.redirectToNew) {
            return <Redirect to='/new'/>
        }

        return <div>
            <Header as='h1'>Employee management app</Header>

            <Grid columns={2} divided>
                <Grid.Row>
                    <Grid.Column>
                        <EmployeeList employeeList={this.props.employeeList}
                                      employeeSelected={this.props.employeeSelected}
                                      selectedEmployee={this.props.selectedEmployee}
                                      deleteEmployee={this.props.deleteEmployee}/>
                    </Grid.Column>
                    <Grid.Column>
                        <EmployeeDetail selectedEmployee={this.props.selectedEmployee}
                                        positions={this.props.positions}
                                        updateEmployee={this.props.updateEmployee}/>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column>
                        <Button className="addNew" primary onClick={this.redirectToNew}>Add new</Button>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </div>;
    }
}