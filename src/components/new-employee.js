import * as React from "react";
import {Redirect} from "react-router-dom";
import EmployeeForm from "./employee-form";
import {Header} from "semantic-ui-react";

export default class NewEmployee extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            dirty: false,
            finished: false,
        }
    }

    addEmployee = (employee) => {
        this.props.addEmployee(employee);
        this.setState({
            finished: true
        })
    };

    validate = () => {
        if (this.state.firstName && this.state.lastName && this.state.dateOfBirth) {
            return true;
        }
        this.setState({error: true});
        return false;
    };

    cancelled = () => {
        this.setState({
            finished: true
        })
    };

    render() {
        if (this.state.finished) {
            return <Redirect to='/'/>
        }

        return (
            <div>
                <Header as='h1'>Add new employee</Header>
                <EmployeeForm submitAction={this.addEmployee} positions={this.props.positions} cancelled={this.cancelled}/>
            </div>
        );
    }
}