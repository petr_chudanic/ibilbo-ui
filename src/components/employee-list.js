import React from 'react';
import EmployeeEntry from './employee-entry';
import {List} from "semantic-ui-react";

export default class EmployeeList extends React.Component {

    render() {
        return <List divided verticalAlign='middle' className="employee-list">
            {this.generateEmployListEntries()}
        </List>
    }

    generateEmployListEntries = () => {
        const employeesList = this.props.employeeList || [];
        return employeesList.map(employee =>
            <EmployeeEntry
                key={employee.getId()}
                selected={employee.getId() === (this.props.selectedEmployee ? this.props.selectedEmployee.getId() : null)}
                employee={employee}
                employeeSelected={this.props.employeeSelected}
                deleteEmployee={this.props.deleteEmployee}/>);
    };
}