import * as React from "react";
import {Form} from "semantic-ui-react";
import {Button, Message} from "semantic-ui-react";
import Employee from "../model/employee";

export default class EmployeeForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dirty: false,
            error: false,
            employee: props.employee,
            firstName: props.employee ? props.employee.getFirstName() : "",
            lastName: props.employee ? props.employee.getLastName() : "",
            workPosition: props.employee ? props.employee.getWorkPosition() : "",
            dateOfBirth: props.employee ? props.employee.getDateOfBirth() : ""
        }
    }

    static getDerivedStateFromProps(props, state) {
        if (state.employee !== props.employee) {
            return {
                ...state,
                employee: props.employee,
                firstName: props.employee ? props.employee.getFirstName() : "",
                lastName: props.employee ? props.employee.getLastName() : "",
                workPosition: props.employee ? props.employee.getWorkPosition() : "",
                dateOfBirth: props.employee ? props.employee.getDateOfBirth() : ""
            };
        }
        return state;
    }

    generateWorkPositions = () => this.props.positions.map(position => <option key={position}
                                                                               value={position}>{position}</option>);

    validate = () => {
        if (this.state.firstName && this.state.lastName && this.state.dateOfBirth) {
            return true;
        }
        this.setState({error: true});
        return false;
    };

    valueChange = (field) =>
        e => {
            const newState = {};
            newState[field] = e.target.value;
            newState["dirty"] = true;
            this.setState(newState);
        };

    onSubmit = (e) => {
        e.preventDefault();
        if (this.validate()) {
            this.props.submitAction(new Employee(this.state.firstName, this.state.lastName, this.state.workPosition, this.state.dateOfBirth));
        }
    };

    cancelled = (e) => {
        e.preventDefault();
        this.props.cancelled();
    };

    render() {
        return (
            <div>
                <Form onSubmit={this.onSubmit} error={this.state.error}>
                    <Form.Field>
                        <label>First Name</label>
                        <input name="firstName" onChange={this.valueChange("firstName")} value={this.state.firstName} />
                        {!this.state.firstName && <Message
                            error
                            content="Missing first name."
                        />}
                    </Form.Field>
                    <Form.Field>
                        <label>Last Name</label>
                        <input name="lastName" onChange={this.valueChange("lastName")} value={this.state.lastName}/>
                        {!this.state.lastName && <Message
                            error
                            content="Missing last name."
                        />}
                    </Form.Field>
                    <Form.Field>
                        <label>Position</label>
                        <select name="workPosition" onChange={this.valueChange("workPosition")} value={this.state.workPosition}>
                            {this.generateWorkPositions()}
                        </select>
                    </Form.Field>
                    <Form.Field>
                        <label>Date of Birth</label>
                        <input name="dateOfBirth" type="date" onChange={this.valueChange("dateOfBirth")} value={this.state.dateOfBirth}/>
                        {!this.state.dateOfBirth && <Message
                            error
                            content="Missing date of birth."
                        />}
                    </Form.Field>
                    <Button type="submit" disabled={!this.state.dirty} primary>Save</Button>
                    {this.props.cancelled && <Button secondary onClick={this.cancelled}>Cancel</Button>}
                </Form>
            </div>
        );
    }
}