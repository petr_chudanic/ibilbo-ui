import React from 'react';
import './employee-entry.css';
import 'semantic-ui/dist/semantic.css';
import {Confirm, List, Button} from "semantic-ui-react";

export default class EmployeeEntry extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            deleteModalVisible: false
        };
    }

    employeeSelected = (e) => {
        e.stopPropagation();
        this.props.employeeSelected(this.props.employee);
    };

    employeeDeleted = (e) => {
        e.stopPropagation();
        this.props.deleteEmployee(this.props.employee);
    };

    employeeDeletionShowConfirmation = () => {
        this.setState({
            deleteModalVisible: true
        })
    };

    employeeDeleteCancelled = () => {
        this.setState({
            deleteModalVisible: false
        })
    };

    render() {
        return <List.Item className={"employeeEntry" + (this.props.selected ? " selected" : "")}
                          onClick={this.employeeSelected}>

            <List.Content floated='right' verticalAlign='middle'>
                <Button onClick={this.employeeDeletionShowConfirmation}>Delete</Button>
            </List.Content>
            <List.Content>
                {this.props.employee.getFirstName() + " " + this.props.employee.getLastName()}
            </List.Content>
            {this.state.deleteModalVisible &&
            <Confirm open={this.state.deleteModalVisible} onCancel={this.employeeDeleteCancelled}
                     onConfirm={this.employeeDeleted}
                     content={`Do you want to delete ${this.props.employee.getFirstName()} ${this.props.employee.getLastName()}?`}/>}
        </List.Item>;
    }
}