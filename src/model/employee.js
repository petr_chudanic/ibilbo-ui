import moment from "moment";

export default class Employee {
    constructor(firstName,
                lastName,
                workPosition,
                dateOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.workPosition = workPosition;
        this.dateOfBirth = moment(dateOfBirth);
    }

    getFirstName() {
        return this.firstName;
    }

    getLastName() {
        return this.lastName;
    }

    getWorkPosition() {
        return this.workPosition;
    }

    getDateOfBirth() {
        return this.dateOfBirth.format("Y-MM-DD");
    }

    getId() {
        return this.firstName + this.lastName;
    }
}